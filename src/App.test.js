import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders main paper', () => {
  const { getByText } = render(<App />);
  const paperHeader = getByText(/used panties/i);
  expect(paperHeader).toBeInTheDocument();
});
